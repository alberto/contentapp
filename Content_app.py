import socket
class webApp:
    def parse(self, request):
        return request.split()[1][1:]

    def compute(self, analyzed):
        request = analyzed.decode('utf-8')
        if request in self.content:
            http = "200 OK"
            html = self.content[request]
        else:
            http="404 error"
            html="<html><body><h1>Error found</h1></body></html>"
        return http, html
    def __init__(self,host,port,content):
        self.content=content
        mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        mySocket.bind((host, port))

        # Queue a maximum of 5 TCP connection requests

        mySocket.listen(5)
        while True:
            print("Waiting for connections")
            (recvSocket, address) = mySocket.accept()
            print("HTTP request received:")

            received = recvSocket.recv(2048)
            print(received)
            petition = self.parse(received)

            http, html = self.compute(petition)
            response = "HTTP/1.1" + http + "\r\n\r\n" \
                       + html \
                       + "\r\n"

            recvSocket.send(response.encode('utf-8'))
            recvSocket.close()

content = {
        'hola': '<html><body><h1>Hola mundo</h1></body></html>',
        'adios': '<html><body><h1>Adios mundo</h1></body></html>',
        '': '<html><body><h1>Vacio</h1></body></html>'}
if __name__=="__main__":
    testAdiosApp=webApp('localhost',1234,content)